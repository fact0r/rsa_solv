-- RSA decryptor in Haskell for TMV210 18/19

module Main where

-- define nA and eA
nA = 667763
eA = 123679

-- pA*qA = nA
pA = 911
qA = 733

-- encrypted message
-- m = 158024

-- phi of nA 
phiNA = (pA -1) * (qA - 1)

-- define alphabet and base for decryption
letters = ['a'..'z']
base = fromIntegral $ length letters

-- brute force test to get inverse of eA in phiNA
dA :: Integer
dA = head $ filter isInverse [1..nA]
  where
    isInverse :: Integer -> Bool
    isInverse y = ((eA * y) `mod` phiNA) == 1

-- decrypt m with dA
mDec :: Integer -> Integer
mDec m = (m ^ dA) `mod` nA

-- decypher message
baseRest :: Integer -> [Integer] -> [Int]
baseRest 0 is = map fromInteger is
baseRest i is = 
  let
    (n, r) = i `quotRem` base
  in 
    baseRest n (r:is)

-- get string from decyphered message
mToString :: Integer -> String
mToString i = reverse $ map (letters!!) (baseRest i [])



main :: IO()
main = do 
  path <- getLine
  text <- readFile path

  let allowedChars = ' ':['0'..'9']
  let cText = [filter (`elem` allowedChars) x | x <- words text]
  let wText = filter ( (>= 4) . length ) cText
  let numbers = map read wText :: [Integer]
  let decNum = map mDec numbers
  let decText = map mToString decNum
  let names = filter ((>=2) . length) $ lines $ filter (`elem` '\n':' ':['A'..'z']) text
  let nameDec = zip names decText
  print nameDec
